import requests
import hashlib
import os
import sys

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

dir = r"./testFiles"
bucketName = sys.argv[1].split("=")[1]
part = sys.argv[3].split("=")[1]
fileName = sys.argv[2].split("=")[1] + ".part{}".format(part)
url = "http://127.0.0.1:5000"
requestUrl = "{}/{}/{}".format(url, bucketName, fileName.rsplit(".", 1)[0])

filePath = "{}/{}".format(dir, fileName)
fileSize = os.path.getsize(filePath)
headers = {'Content-Length': str(fileSize), 'Content-MD5': md5(filePath)}
params = {'partNumber': part}
with open(filePath, 'rb') as f:
    r = requests.put(requestUrl, data=f, headers=headers, params=params)
print(r, r.content)