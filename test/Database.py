import mysql.connector

dbConfig = {
    'user': 'root',
    'password': '',
    'host': '127.0.0.1',
    'database': 'object_storage'
}

def connectDB():
    return mysql.connector.connect(**dbConfig)

def allTablesAreEmpty():
    try:
        conn = connectDB()
        cur = conn.cursor()
        query = ("SELECT table_schema, table_type, table_name "
                 "FROM information_schema.tables "
                 "WHERE table_rows >= 1 AND TABLE_SCHEMA='object_storage'")
        cur.execute(query)
        tablesWithRow = cur.fetchall()
        if tablesWithRow: return False
        else: return True
    finally:
        cur.close()
        conn.close()

def getAllBucketNames():
    try:
        conn = connectDB()
        cur = conn.cursor()
        query = ("SELECT bucket_name FROM buckets")
        cur.execute(query)
        bucketNames = []
        for row in cur:
            bucketNames.append(row[0])
        return bucketNames
    finally:
        cur.close()
        conn.close()