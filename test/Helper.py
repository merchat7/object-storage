import test.Database as db
import test.Request as Request
import os
import hashlib

def cleanUp(bucketsLocation):
    for bucketName in db.getAllBucketNames():
        Request.deleteBucket(bucketName)
    assert allCleared(bucketsLocation)

def allCleared(bucketsLocation):
    files = os.listdir(bucketsLocation)
    return (db.allTablesAreEmpty() and
            len(files) == 1 and
            str(files[0]) == ".gitignore")

def generateObjectMD5FromParts(md5Parts):
    md5String = ""
    for md5Part in md5Parts:
        md5String += md5Part
    return hashlib.md5(md5String.encode('utf8')).hexdigest() + "-{}".format(len(md5Parts))