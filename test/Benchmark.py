from locust import HttpLocust, TaskSet, task
import os
import hashlib
import requests
import random
import mysql.connector

dbConfig = {
    'user': 'root',
    'password': '',
    'host': '127.0.0.1',
    'database': 'object_storage'
}

def getAllBucketNames():
    try:
        conn = connectDB()
        cur = conn.cursor()
        query = ("SELECT bucket_name FROM buckets")
        cur.execute(query)
        bucketNames = []
        for row in cur:
            bucketNames.append(row[0])
        return bucketNames
    finally:
        cur.close()
        conn.close()

def connectDB():
    return mysql.connector.connect(**dbConfig)

def cleanUp():
    for bucketName in getAllBucketNames():
        deleteBucket(bucketName)

def deleteBucket(bucketName):
    r = requests.delete(url + bucketName, params={'delete': ''})
    return r

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

url = 'http://127.0.0.1:5000/'

class SimpleTasks(TaskSet):
    def setup(self):
        cleanUp()
        requests.post(url + "myBucket", params={'create': ''})

    def teardown(self):
        cleanUp()

    @task
    def fileTask(self):
        bucketName = "myBucket"
        objectName = "hello"
        part = 1
        requestUrl = url + "{}/{}".format(bucketName, objectName + str(random.randint(0, 100000000)))
        # Create upload ticket
        self.client.post(requestUrl, params={'create': ''})
        # Upload the part
        dir = r"./testFiles"
        filePath = "{}/{}".format(dir, objectName + ".part{}".format(part))
        fileSize = os.path.getsize(filePath)
        md5File = md5(filePath)
        headers = {'Content-Length': str(fileSize), 'Content-MD5': md5File}
        with open(filePath, 'rb') as f:
            self.client.put(requestUrl, data=f, headers=headers, params={'partNumber': part})
        # Complete upload ticket
        self.client.post(requestUrl, params={'complete': ''})
        # Add some metadata
        key = "key"
        value = "value"
        for _ in range(10):
            self.client.put(requestUrl, params={'metadata' : '', 'key': key + str(random.randint(0, 100000000))}, data={'value': value})
        # Get all the metadata
        self.client.get(requestUrl, params={'metadata' : ''})
        # Delete the object
        self.client.delete(requestUrl, params={'delete': ''})

class BasicBenchmark(HttpLocust):
    task_set = SimpleTasks
    host = url
