import test.Helper as Helper
import test.Request as Request

# Enable case sensitivty in Windows https://blogs.msdn.microsoft.com/commandline/2018/02/28/per-directory-case-sensitivity-and-wsl/
# Downloading of objects should be manually tested
# TODO: Ensure all errors is correct and make an error title/code for easy testing
# TODO: Test more advance use cases

bucketsLocation = r"C:\Users\Hart\PycharmProjects\Backend\HW0\ObjectStorage\app\buckets"

def testBasicBucket():
    Helper.cleanUp(bucketsLocation)
    # Create bucket called "myBucket"
    bucketName = "myBucket"
    r = Request.createBucket(bucketName)
    assert (r.status_code == 200 and
            r.json()['created'] and
            r.json()['modified'] and
            r.json()['name'] == bucketName)
    # Try creating bucket "myBucket" again (should fail)
    r = Request.createBucket(bucketName)
    assert r.status_code == 400
    # List bucket content (should be empty)
    r = Request.listBucket(bucketName)
    assert (r.status_code == 200 and
            not r.json()['objects'] and
            bucketName == r.json()['bucket']['name'])
    # Create the upload ticket for object "abc"
    objectName = "abc"
    r = Request.createUploadTicket(bucketName, objectName)
    assert r.status_code == 200
    # Upload the part
    part = 1
    r, firstMD5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            firstMD5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    # List bucket content (should still be empty as object is not yet complete)
    r = Request.listBucket(bucketName)
    assert (r.status_code == 200 and
            not r.json()['objects'])
    # Complete the upload
    r = Request.completeUploadTicket(bucketName, objectName)
    assert (r.status_code == 200 and
            Helper.generateObjectMD5FromParts([firstMD5File]) == r.json()['eTag'] and
            fileSize == r.json()['length'] and
            objectName == r.json()['name'])
    # Create the upload ticket for object "hello"
    objectName = "hello"
    r = Request.createUploadTicket(bucketName, objectName)
    assert r.status_code == 200
    # Upload the part
    part = 1
    r, secondMD5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            secondMD5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    # Complete the upload
    r = Request.completeUploadTicket(bucketName, objectName)
    assert (r.status_code == 200 and
            Helper.generateObjectMD5FromParts([secondMD5File]) == r.json()['eTag'] and
            fileSize == r.json()['length'] and
            objectName == r.json()['name'])
    # List bucket content (should be two objects)
    r = Request.listBucket(bucketName)
    assert (r.status_code == 200 and
            "abc" == r.json()['objects'][0]['name'] and
            Helper.generateObjectMD5FromParts([firstMD5File]) == r.json()['objects'][0]['eTag'] and
            "hello" == r.json()['objects'][1]['name'] and
            Helper.generateObjectMD5FromParts([secondMD5File]) == r.json()['objects'][1]['eTag'])
    # Delete the object, "abc"
    objectName = "abc"
    r = Request.deleteObject(bucketName, objectName)
    assert r.status_code == 200
    # List bucket content (should be one object)
    r = Request.listBucket(bucketName)
    assert (r.status_code == 200 and
            "hello" == r.json()['objects'][0]['name'] and
            Helper.generateObjectMD5FromParts([secondMD5File]) == r.json()['objects'][0]['eTag'])
    # Delete an non-existing bucket (also test case sensitivity)
    r = Request.deleteBucket(bucketName.capitalize())
    assert r.status_code == 400
    # Delete the bucket
    r = Request.deleteBucket(bucketName)
    assert r.status_code == 200
    # Ensure everything was cleaned up
    assert Helper.allCleared(bucketsLocation)

def testSinglePartUpload():
    Helper.cleanUp(bucketsLocation)
    # Create bucket called "myBucket"
    bucketName = "myBucket"
    r = Request.createBucket(bucketName)
    assert (r.status_code == 200 and
            r.json()['created'] and
            r.json()['modified'] and
            r.json()['name'] == bucketName)
    # Upload the part (should error)
    objectName = "abc"
    part = 1
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 400)
    # Create the upload ticket for object "abc"
    objectName = "abc"
    r = Request.createUploadTicket(bucketName, objectName)
    assert r.status_code == 200
    # Try creating upload ticket again for "abc" (should error)
    objectName = "abc"
    r = Request.createUploadTicket(bucketName, objectName)
    assert r.status_code == 400
    # Upload the part again
    part = 1
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            md5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    # Delete the part
    r = Request.deleteObjectPart(bucketName, objectName, part)
    assert r.status_code == 200
    # Delete a valid but non-existing part
    r = Request.deleteObjectPart(bucketName, objectName, 1000)
    assert r.status_code == 200
    # Reupload the part
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            md5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    # Reupload the part once more (should fail as already exist and md5 matches)
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert r.status_code == 400
    # Complete the upload
    r = Request.completeUploadTicket(bucketName, objectName)
    assert (r.status_code == 200 and
            Helper.generateObjectMD5FromParts([md5File]) == r.json()['eTag'] and
            fileSize == r.json()['length'] and
            objectName == r.json()['name'])
    # Try creating upload ticket again for "abc"
    objectName = "abc"
    r = Request.createUploadTicket(bucketName, objectName)
    assert r.status_code == 400
    # Delete the part again (should error)
    r = Request.deleteObjectPart(bucketName, objectName, part)
    assert r.status_code == 400
    # Delete non existing object (test case sensitivity)
    r = Request.deleteObject(bucketName, objectName.capitalize())
    assert r.status_code == 400
    # Delete the object
    r = Request.deleteObject(bucketName, objectName)
    assert r.status_code == 200
    # Delete the bucket
    r = Request.deleteBucket(bucketName)
    assert r.status_code == 200
    # Ensure everything was cleaned up
    assert Helper.allCleared(bucketsLocation)

def testMultiPartUpload():
    Helper.cleanUp(bucketsLocation)
    # Create bucket called "myBucket"
    bucketName = "myBucket"
    r = Request.createBucket(bucketName)
    assert (r.status_code == 200 and
            r.json()['created'] and
            r.json()['modified'] and
            r.json()['name'] == bucketName)
    # Create the upload ticket for object "testFile.txt"
    objectName = "testFile.txt"
    r = Request.createUploadTicket(bucketName, objectName)
    assert r.status_code == 200
    # Upload the parts
    md5Parts = []
    totalFileSize = 0
    part = 1
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            md5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    md5Parts.append(md5File)
    totalFileSize += fileSize
    part = 2
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            md5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    md5Parts.append(md5File)
    totalFileSize += fileSize
    part = 3
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            md5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    md5Parts.append(md5File)
    totalFileSize += fileSize
    part = 4
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            md5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    md5Parts.append(md5File)
    totalFileSize += fileSize
    # Complete the upload
    r = Request.completeUploadTicket(bucketName, objectName)
    assert (r.status_code == 200 and
            Helper.generateObjectMD5FromParts(md5Parts) == r.json()['eTag'] and
            totalFileSize == r.json()['length'] and
            objectName == r.json()['name'])
    # Delete the bucket
    r = Request.deleteBucket(bucketName)
    assert r.status_code == 200
    # Ensure everything was cleaned up
    assert Helper.allCleared(bucketsLocation)

def testObjectMetadata():
    Helper.cleanUp(bucketsLocation)
    # Create bucket called "myBucket"
    bucketName = "myBucket"
    r = Request.createBucket(bucketName)
    assert (r.status_code == 200 and
            r.json()['created'] and
            r.json()['modified'] and
            r.json()['name'] == bucketName)
    # Create the upload ticket for object "hello"
    objectName = "hello"
    r = Request.createUploadTicket(bucketName, objectName)
    assert r.status_code == 200
    # Upload the part
    part = 1
    r, md5File, fileSize = Request.uploadFile(bucketName, objectName, part)
    assert (r.status_code == 200 and
            md5File == r.json()['md5'] and
            fileSize == r.json()['length'] and
            str(part) == r.json()['partNumber'])
    # Set object metadata before object upload is complete (should fail)
    key = "key1"
    value = "value1"
    r = Request.setObjectMeta(bucketName, objectName, key, value)
    assert r.status_code == 400
    # Complete the upload
    r = Request.completeUploadTicket(bucketName, objectName)
    assert (r.status_code == 200 and
            Helper.generateObjectMD5FromParts([md5File]) == r.json()['eTag'] and
            fileSize == r.json()['length'] and
            objectName == r.json()['name'])
    # Set object metadata for non existing bucket (test case sensitivity)
    key = "key1"
    value = "value1"
    r = Request.setObjectMeta(bucketName.capitalize(), objectName, key, value)
    assert r.status_code == 400
    # Set object metadata for non existing object (test case sensitivity)
    key = "key1"
    value = "value1"
    r = Request.setObjectMeta(bucketName, objectName.capitalize(), key, value)
    assert r.status_code == 400
    # Set object metadata
    key = "key1"
    value = "value1"
    r = Request.setObjectMeta(bucketName, objectName, key, value)
    assert (r.status_code == 200)
    key = "key2"
    value = "value2"
    r = Request.setObjectMeta(bucketName, objectName, key, value)
    assert (r.status_code == 200)
    key = "key3"
    value = "value3"
    r = Request.setObjectMeta(bucketName, objectName, key, value)
    assert (r.status_code == 200)
    # Get all object metadatas
    r = Request.getAllObjectMetas(bucketName, objectName)
    assert (r.status_code == 200 and
            "value1" == r.json()['metas']["key1"] and
            "value2" == r.json()['metas']["key2"] and
            "value3" == r.json()['metas']["key3"])
    # Update existing metadata
    key = "key1"
    value = "updatedValue1"
    r = Request.setObjectMeta(bucketName, objectName, key, value)
    # Delete object metadata (test case sensitivity)
    key = "key1"
    r = Request.removeObjectMeta(bucketName, objectName, key.capitalize())
    assert (r.status_code == 200)
    # Delete object metadata
    key = "key2"
    r = Request.removeObjectMeta(bucketName, objectName, key)
    assert (r.status_code == 200)
    # Delete non-existing object metadata
    key = "key2"
    r = Request.removeObjectMeta(bucketName, objectName, key)
    assert (r.status_code == 200)
    # Get object metadata by key
    key = "key3"
    value = "value3"
    r = Request.getObjectMetaByKey(bucketName, objectName, key)
    assert (r.status_code == 200 and
            value == r.json()['metas'][key])
    # Get object metadata by key (non-existing)
    key = "key1000"
    r = Request.getObjectMetaByKey(bucketName, objectName, key)
    assert (r.status_code == 200 and
            not r.json()['metas'])
    # Get all object metadatas
    r = Request.getAllObjectMetas(bucketName, objectName)
    assert (r.status_code == 200 and
            "updatedValue1" == r.json()['metas']["key1"] and
            "value3" == r.json()['metas']["key3"])
    # Delete the bucket
    r = Request.deleteBucket(bucketName)
    assert r.status_code == 200
    # Ensure everything was cleaned up
    assert Helper.allCleared(bucketsLocation)
