import hashlib
import Response as res
import re
import os
import Database as db
import Helper

validBucketNamePattern = r"^[a-zA-Z0-9_\-]+$" # alphanumeric + underscore + hyphen
validObjectNamePattern = r"^[a-zA-Z0-9_\-\.]+$" # alphanumeric + underscore + hyphen + dot

def checkObjectStorageHandlerArgs(bucketName, objectName, request, params, method):
    if bucketName is not None and not re.match(validBucketNamePattern, bucketName):
        return res.makeForbiddenNameResponse("Bucket")
    if params is not None:
        if not any(param in request.args for param in params) and method != "GET_OBJECT":
            return res.makeMissingRequiredResponse("param", params)
    paramsCheckFailed = additionalCheckParams(request, method)
    if paramsCheckFailed: return paramsCheckFailed
    if objectName is not None and ("." in (objectName[0], objectName[-1]) or not re.match(validObjectNamePattern, objectName)):
        return res.makeForbiddenNameResponse("Object")

def additionalCheckParams(request, method):
    if "partNumber" in request.args:
        try: partNumber = int(request.args['partNumber'])
        except ValueError: return res.partNumberNotANumber
        else:
            if not (1 <= partNumber <= 10000): return res.partNumberOutOfRange
    objectMetaParams = ["metadata", "key"]
    if any(param in request.args for param in objectMetaParams):
        if "key" in request.args:
            keyValue = request.args["key"]
            if not keyValue.strip(): return res.makeMissingRequiredResponse("value for param(key)", "<empty>")
        missingParams = []
        for objectMetaParam in objectMetaParams:
            if objectMetaParam not in request.args: missingParams.append(objectMetaParam)
        if method == "GET_OBJECT":
            if "metadata" in missingParams: return res.makeMissingRequiredResponse("param", "metadata")
        else:
            if missingParams: return res.makeMissingRequiredResponse("param", missingParams)
            if method == "PUT":
                if "value" not in request.form: return res.makeMissingRequiredResponse("body", "value")
                valValue = request.form["value"]
                if not valValue.strip(): return res.makeMissingRequiredResponse("value for body(value)", "<empty>")

def constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=False, preventIfObjectIsDeleted=False, preventIfObjectIsComplete=False, preventIfObjectIsIncomplete=False):
    if preventIfBucketIsDeleted:
        isDeletedBucket = checkIfAlreadyDeleted(bucketName, None)
        if isDeletedBucket: return isDeletedBucket
    if preventIfObjectIsDeleted:
        isDeletedObject = checkIfAlreadyDeleted(bucketName, objectName)
        if isDeletedObject: return isDeletedObject
    if preventIfObjectIsComplete or preventIfObjectIsIncomplete:
        isCompleteObject = checkIfAlreadyComplete(bucketName, objectName)
        if preventIfObjectIsIncomplete and isCompleteObject != res.uploadAlreadyDone:
            return isCompleteObject
        if preventIfObjectIsComplete and isCompleteObject != res.uploadNotDone:
            return isCompleteObject

def checkHeader(request, header):
    if header not in request.headers:
        return res.makeMissingRequiredResponse("header", header)

def checkUploadRequest(request):
    toCheck = ("Content-Length", "Content-MD5")
    for header in toCheck:
        result = checkHeader(request, header)
        if result: return result

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def checkFileIntegrity(filePath, request):
    myRes = None
    fileSize = os.path.getsize(filePath)
    md5File =  md5(filePath)
    if int(fileSize) != int(request.headers['Content-Length']):
        myRes = res.makeMismatchedResponse("Content-Length")
    elif md5File != request.headers['Content-MD5']:
        myRes = res.makeMismatchedResponse("Content-MD5")
    return myRes, fileSize, md5File

def checkIfAlreadyDeleted(bucketName, objectName):
    if objectName is None:
        key = "Bucket"
        deleted = db.getBucketDeleteFlag(bucketName)
    else:
        key = "Object or upload ticket"
        deleted = db.getObjectDeleteFlag(bucketName, objectName)
    # No bucket record or no object record found
    if deleted is None:
        return res.makeDoesNotExistResponse(key)
    # Delete flag is set for bucket/object
    elif deleted:
        return res.makeAlreadyDeletedResponse(key)

def checkIfAlreadyComplete(bucketName, objectName):
    completed = db.getObjectCompleteFlag(bucketName, objectName)
    if completed is None:
        return res.makeDoesNotExistResponse("Object or upload ticket")
    elif completed:
        return res.uploadAlreadyDone
    elif not completed:
        return res.uploadNotDone

def checkIfMissingParts(partNumbers):
    missingParts = []
    totalParts = max(partNumbers)
    # Assume biggest part number is the final part
    if len(partNumbers) != totalParts:
        indexParts = 0
        for n in range(1, totalParts+1):
            if partNumbers[indexParts] == n:
                indexParts += 1
            else:
                missingParts.append(n)
    if missingParts: return res.makeMissingPartsResponse(missingParts)

def checkPartsMD5(bucketName, objectName, partNumbers, md5Parts):
    if md5Parts is None: return res.noPartsUploaded # User should attempt to upload again
    if len(partNumbers) != len(md5Parts): return res.missingPartsMD5
    partsMismatchedMD5 = []
    for partNumber, md5Part in zip(partNumbers,md5Parts):
        objectPathAbs = Helper.makeAbsPath(bucketName, Helper.addPartToObjectName(objectName, str(partNumber)))
        if md5(objectPathAbs) != md5Part:
            partsMismatchedMD5.append(partNumber)
    if partsMismatchedMD5: return res.makePartsMD5MismatchedResponse(partsMismatchedMD5)

def checkObjectParts(bucketName, objectName, objectParts, md5Parts):
    if not objectParts: return res.noPartsUploaded
    partNumbers = [Helper.getPartNum(objectName, objectPart) for objectPart in objectParts]
    isMissing = checkIfMissingParts(partNumbers)
    if isMissing: return isMissing
    md5Error = checkPartsMD5(bucketName, objectName, partNumbers, md5Parts)
    if md5Error: return md5Error


