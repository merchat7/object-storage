import mysql.connector
from mysql.connector import errorcode, pooling
import copy
import sys
import Init
import time

dbConfig = {
    'user': 'root',
    'password': 'test',
    'host': 'db'
}

isLocal = False
if len(sys.argv) >= 2 and sys.argv[1] == "local":
    isLocal = True
    dbConfig['host'] = "127.0.0.1"

dbName = "object_storage"
dbConfigWithDB = copy.deepcopy(dbConfig)
dbConfigWithDB['database'] = dbName

tables = {}
tables['buckets'] = (
    "CREATE TABLE `buckets` ("
    "  `bucket_name` VARCHAR(100) BINARY NOT NULL,"
    "  `delete_flag` BOOL NOT NULL,"
    "  PRIMARY KEY (`bucket_name`)"
    ") ENGINE=InnoDB")
tables['buckets_meta'] = (
    "CREATE TABLE `buckets_meta` ("
    "  `bucket_name` VARCHAR(100) BINARY NOT NULL,"
    "  `created` BIGINT(11) NOT NULL,"
    "  `modified` BIGINT(11) NOT NULL,"
    "  PRIMARY KEY (`bucket_name`),"
    "  FOREIGN KEY (`bucket_name`) REFERENCES buckets(`bucket_name`) ON DELETE CASCADE"
    ") ENGINE=InnoDB")
tables['objects'] = (
    "CREATE TABLE `objects` ("
    "  `object_name` VARCHAR(100) BINARY NOT NULL,"
    "  `bucket_name` VARCHAR(100) BINARY NOT NULL,"
    "  `md5` VARCHAR(40) NULL DEFAULT NULL,"
    "  `complete_flag` BOOL NOT NULL,"
    "  `delete_flag` BOOL NOT NULL,"
    "  PRIMARY KEY (`object_name`, `bucket_name`),"
    "  FOREIGN KEY (`bucket_name`) REFERENCES buckets(`bucket_name`) ON DELETE CASCADE"
    ") ENGINE=InnoDB")
tables['objects_parts'] = (
    "CREATE TABLE `objects_parts` ("
    "  `object_name` VARCHAR(100) BINARY NOT NULL,"
    "  `bucket_name` VARCHAR(100) BINARY NOT NULL,"
    "  `part_number` INT(5) NOT NULL,"
    "  `md5` VARCHAR(40) NOT NULL,"
    "  PRIMARY KEY (`object_name`, `bucket_name`, `part_number`),"
    "  FOREIGN KEY (`object_name`) REFERENCES objects(`object_name`) ON DELETE CASCADE,"
    "  FOREIGN KEY (`bucket_name`) REFERENCES buckets(`bucket_name`) ON DELETE CASCADE"
    ") ENGINE=InnoDB")
tables['objects_meta'] = (
    "CREATE TABLE `objects_meta` ("
    "  `object_name` VARCHAR(100) BINARY NOT NULL,"
    "  `bucket_name` VARCHAR(100) BINARY NOT NULL,"
    "  `key` VARCHAR(100) BINARY NOT NULL,"
    "  `value` VARCHAR(500) NOT NULL,"
    "  PRIMARY KEY (`object_name`, `bucket_name`, `key`),"
    "  FOREIGN KEY (`object_name`) REFERENCES objects(`object_name`) ON DELETE CASCADE,"
    "  FOREIGN KEY (`bucket_name`) REFERENCES buckets(`bucket_name`) ON DELETE CASCADE"
    ") ENGINE=InnoDB")

def connectRootDB():
    return mysql.connector.connect(**dbConfig)

def createDB():
    conn = connectRootDB()
    cur = conn.cursor()
    query = "CREATE DATABASE IF NOT EXISTS {}".format(dbName)
    cur.execute(query)
    cur.close()
    conn.close()

def getConnFromPool():
    try:
        return dbPool.get_connection()
    except mysql.connector.PoolError:
        return connectDB()

def connectDB():
    return mysql.connector.connect(**dbConfigWithDB)

def createTables():
    conn = getConnFromPool()
    cur = conn.cursor()
    for name, query in tables.items():
        try:
            print("Creating table {}: ".format(name), end='')
            cur.execute(query)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("Already exists")
            else:
                print(err.msg)
        else:
            print("OK")
    cur.close()
    conn.close()

dbPool = None
if isLocal: Init.init()
else: # On prod, use the init.sql to create database schema
    while True:
        try:
            print("Trying to set up database pool")
            dbPool = pooling.MySQLConnectionPool(pool_name = "myPool",
                                                 pool_size = 10, # Assume each thread will produce 10 connections each
                                                 **dbConfigWithDB)
        except Exception as e:
            print(str(e))
            print("Retrying...")
            time.sleep(1)
        else:
            break

def addBucketToDB(bucketName, created, modified):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryBuckets = (
            "INSERT INTO buckets "
            "VALUES (%s, %s)"
        )
        deleteFlag = 0
        cur.execute(queryBuckets, (bucketName, deleteFlag))

        queryBucketsMeta = (
            "INSERT INTO buckets_meta "
            "VALUES (%s, %s, %s)"
        )
        cur.execute(queryBucketsMeta, (bucketName, created, modified))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def addUploadTicketToDB(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryUploadTicket = (
            "INSERT INTO objects "
            "VALUES (%s, %s, DEFAULT, %s, %s)"
        )
        completeFlag, deleteFlag = 0, 0
        cur.execute(queryUploadTicket, (objectName, bucketName, completeFlag, deleteFlag))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def addObjectPartToDB(bucketName, objectName, part, md5Value):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryObjectPart = (
            "INSERT INTO objects_parts "
            "VALUES (%s, %s, %s, %s) "
            "ON DUPLICATE KEY UPDATE md5=%s"
        )
        cur.execute(queryObjectPart, (objectName, bucketName, part, md5Value, md5Value))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def addObjectMetaToDB(bucketName, objectName, key, value):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryObjectMeta = (
            "INSERT INTO objects_meta "
            "VALUES (%s, %s, %s, %s) "
            "ON DUPLICATE KEY UPDATE value=%s"
        )
        cur.execute(queryObjectMeta, (objectName, bucketName, key, value, value))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def setBucketDeleteFlag(bucketName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryDelete = (
            "UPDATE buckets "
            "SET delete_flag=1 "
            "WHERE bucket_name=%s"
        )
        cur.execute(queryDelete, (bucketName,))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def setObjectDeleteFlag(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryDelete = (
            "UPDATE objects "
            "SET delete_flag=1 "
            "WHERE bucket_name=%s AND object_name=%s"
        )
        cur.execute(queryDelete, (bucketName, objectName))
        conn.commit()
    finally:
        cur.close()
        conn.close()

# Set complete flag & add the md5
def setObjectComplete(bucketName, objectName, md5Object):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryComplete = (
            "UPDATE objects "
            "SET complete_flag=1, md5=%s "
            "WHERE bucket_name=%s AND object_name=%s"
        )
        cur.execute(queryComplete, (md5Object, bucketName, objectName))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def getBucketDeleteFlag(bucketName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT delete_flag FROM buckets "
            "WHERE bucket_name=%s"
        )
        cur.execute(queryGet, (bucketName,))
        deleteFlag = cur.fetchone()
        if deleteFlag: return deleteFlag[0]
    finally:
        cur.close()
        conn.close()

def getObjectDeleteFlag(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT delete_flag FROM objects "
            "WHERE bucket_name=%s AND object_name=%s"
        )
        cur.execute(queryGet, (bucketName, objectName))
        deleteFlag = cur.fetchone()
        if deleteFlag: return deleteFlag[0]
    finally:
        cur.close()
        conn.close()

def getObjectCompleteFlag(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT complete_flag FROM objects "
            "WHERE bucket_name=%s AND object_name=%s"
        )
        cur.execute(queryGet, (bucketName, objectName))
        completeFlag = cur.fetchone()
        if completeFlag: return completeFlag[0]
    finally:
        cur.close()
        conn.close()

def getObjectMD5(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT md5 FROM objects "
            "WHERE bucket_name=%s AND object_name=%s"
        )
        cur.execute(queryGet, (bucketName, objectName))
        md5Object = cur.fetchone()
        if md5Object: return md5Object[0]
    finally:
        cur.close()
        conn.close()

def getObjectPartsMD5(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT md5 FROM objects_parts "
            "WHERE bucket_name=%s AND object_name=%s "
            "ORDER BY part_number"
        )
        cur.execute(queryGet, (bucketName, objectName))
        md5Parts = []
        for row in cur: md5Parts.append(row[0])
        if md5Parts: return md5Parts
    finally:
        cur.close()
        conn.close()

def getObjectInBucket(bucketName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT object_name, md5 FROM objects "
            "WHERE bucket_name=%s AND complete_flag=1"
        )
        cur.execute(queryGet, (bucketName,))
        objects = []
        for row in cur:
            object = {"name": row[0], "eTag": row[1]}
            objects.append(object)
        return objects
    finally:
        cur.close()
        conn.close()

def getObjectMetaByKey(bucketName, objectName, key):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT `value` FROM objects_meta "
            "WHERE bucket_name=%s AND object_name=%s AND `key`=%s"
        )
        cur.execute(queryGet, (bucketName, objectName, key))
        toReturn = {}
        value = cur.fetchone()
        if value: toReturn = {key:value[0]}
        return toReturn
    finally:
        cur.close()
        conn.close()

def getAllObjectMetas(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryGet = (
            "SELECT `key`, `value` FROM objects_meta "
            "WHERE bucket_name=%s AND object_name=%s"
        )
        cur.execute(queryGet, (bucketName, objectName))
        values = {}
        for row in cur:
            values[row[0]] = row[1]
        return values
    finally:
        cur.close()
        conn.close()

def removeBucketFromDB(bucketName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryDelete = (
            "DELETE FROM buckets "
            "WHERE bucket_name=%s"
        )
        cur.execute(queryDelete, (bucketName,))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def removeObjectFromDB(bucketName, objectName):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryDelete = (
            "DELETE FROM objects "
            "WHERE bucket_name=%s AND object_name=%s"
        )
        cur.execute(queryDelete, (bucketName, objectName))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def removeObjectPartFromDB(bucketName, objectName, part):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryDelete = (
            "DELETE FROM objects_parts "
            "WHERE bucket_name=%s AND object_name=%s AND part_number=%s"
        )
        cur.execute(queryDelete, (bucketName, objectName, part))
        conn.commit()
    finally:
        cur.close()
        conn.close()

def removeObjectMetaFromDB(bucketName, objectName, key):
    try:
        conn = getConnFromPool()
        cur = conn.cursor()
        queryDelete = (
            "DELETE FROM objects_meta "
            "WHERE bucket_name=%s AND object_name=%s AND `key`=%s"
        )
        cur.execute(queryDelete, (bucketName, objectName, key))
        conn.commit()
    finally:
        cur.close()
        conn.close()