import os
from Global import storagePath, databasePath
from mysql.connector import pooling
import Database as db

def init():
    print("Initialization start")
    print("Creating the database")
    db.createDB()
    db.dbPool = pooling.MySQLConnectionPool(pool_name = "myPool",
                                            pool_size = 10, # Assume each thread will produce 10 connections each
                                            **db.dbConfigWithDB)
    print("Creating the tables")
    db.createTables()
    print("Creating required directory")
    if not os.path.exists(storagePath): os.makedirs(storagePath)
    if not os.path.exists(databasePath): os.makedirs(databasePath)
    print("Initialization complete")