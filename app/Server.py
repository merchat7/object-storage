from flask import Flask, request
import logging
import Bucket
import Verifier

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

app = Flask(__name__)

@app.route('/<bucketName>', methods=['POST'])
def handleCreateBucket(bucketName):
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(bucketName, None, request, ["create"], "POST")
    if invalidArgument: return invalidArgument
    result = Bucket.createBucket(bucketName)
    return result

@app.route('/<bucketName>', methods=['DELETE'])
def handleDeleteBucket(bucketName):
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(bucketName, None, request, ["delete"], "DELETE")
    if invalidArgument: return invalidArgument
    result = Bucket.deleteBucket(bucketName)
    return result

@app.route('/<bucketName>') # methods=['GET']
def handleListBucket(bucketName):
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(bucketName, None, request, ["list"], "GET_BUCKET")
    if invalidArgument: return invalidArgument
    result = Bucket.listBucket(bucketName)
    return result

@app.route('/<bucketName>/<objectName>', methods=['POST'])
def handleUploadTicket(bucketName, objectName):
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(bucketName, objectName, request, ["create", "complete"], "POST")
    if invalidArgument: return invalidArgument
    if "create" in request.args: result = Bucket.createUploadTicket(bucketName, objectName)
    else: result = Bucket.completeUploadTicket(bucketName, objectName)
    return result

@app.route('/<bucketName>/<objectName>', methods=['PUT'])
def handleObjectPut(bucketName, objectName):
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(bucketName, objectName, request, ["partNumber", "metadata", "key"], "PUT")
    if invalidArgument: return invalidArgument
    if "partNumber" in request.args:
        missingHeader = Verifier.checkUploadRequest(request)
        if missingHeader: return missingHeader
        result = Bucket.saveObjectPartToBucket(bucketName, objectName, request.data, request.args['partNumber'], request)
    else:
        result = Bucket.saveObjectMeta(bucketName, objectName, request)
    return result

@app.route('/<bucketName>/<objectName>', methods=['DELETE'])
def handleObjectDelete(bucketName, objectName):
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(bucketName, objectName, request, ["partNumber", "delete", "metadata", "key"], "DELETE")
    if invalidArgument: return invalidArgument
    if "partNumber" in request.args: result = Bucket.deleteObjectPartFromBucket(bucketName, objectName, request.args['partNumber'])
    elif "delete" in request.args: result = Bucket.deleteObjectFromBucket(bucketName, objectName)
    else: result = Bucket.deleteObjectMeta(bucketName, objectName, request)
    return result

@app.route('/<bucketName>/<objectName>') # methods=['GET']
def handleObjectGet(bucketName, objectName):
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(bucketName, objectName, request, ["metadata", "key"], "GET_OBJECT")
    if invalidArgument: return invalidArgument
    if "key" in request.args: result = Bucket.getObjectMetaByKey(bucketName, objectName, request.args['key'])
    elif "metadata" in request.args: result = Bucket.getAllObjectMetas(bucketName, objectName)
    else: result = Bucket.download(bucketName, objectName, request)
    return result

@app.route('/') # methods=['GET']
def handleListAllBuckets():
    invalidArgument = Verifier.checkObjectStorageHandlerArgs(None, None, request, ["list"], "GET_ALL_BUCKETS")
    if invalidArgument: return invalidArgument
    result = Bucket.listAllBuckets()
    return result

if __name__ == '__main__':
    app.run(host = '0.0.0.0', threaded=True)