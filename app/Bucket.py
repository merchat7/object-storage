from Global import storagePath
import Database as db
from mysql.connector import Error as mysqlError, errorcode
import Response as res
import Verifier
import Helper
import os
import shutil
import time
from flask import Response

# TODO: Adjust bucket's modified time after certain operations
# TODO: Add object's created and modified time (if necessary)

def createBucket(bucketName):
    bucketPath = "{}/{}".format(storagePath, bucketName)
    # If folder is created, this means no error occurred when adding entry to DB
    if os.path.exists(bucketPath):
        return res.makeAlreadyExistResponse("Bucket")
    try:
        created = int(time.time())
        modified = created
        db.addBucketToDB(bucketName, created, modified)
        os.makedirs(bucketPath) # If error on above function, won't execute
        return res.makeResponse(200,
                                {'status': "Success",
                                 'created': created,
                                 'modified': modified,
                                 'name': bucketName})
    except mysqlError as e:
        statusCode, status, msg = 400, "Error", e.msg
        if e.errno == errorcode.ER_DUP_ENTRY:
            # Bucket was removed from file system, but not DB, take care of it here rather than deleteBucket
            try:
                db.removeBucketFromDB(bucketName)
                statusCode, status, msg = 409, "Conflict", "Redundant record removed, try again"
            except: pass
        return res.makeErrorResponse(statusCode, status, msg)
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def deleteBucket(bucketName):
    bucketPath = "{}/{}".format(storagePath, bucketName)
    if not os.path.exists(bucketPath):
        # If bucket was deleted from file system, but not DB, take care of it in createBucket (minor optimization)
        return res.makeDoesNotExistResponse("Bucket")
    try:
        # Prevent anymore client from connecting to bucket
        db.setBucketDeleteFlag(bucketName)
        # Now remove the bucket
        shutil.rmtree(bucketPath)
        db.removeBucketFromDB(bucketName)
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def listBucket(bucketName):
    bucketPath = "{}/{}".format(storagePath, bucketName)
    if not os.path.exists(bucketPath):
        return res.makeDoesNotExistResponse("Bucket")
    try:
        bucket = {'name' : bucketName}
        objects = db.getObjectInBucket(bucketName)
        return res.makeResponse(200,
                                {'status': "Success",
                                 'bucket': bucket,
                                 'objects': objects})
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def listAllBuckets():
    try:
        buckets = [file for file in os.listdir(storagePath) if not file.startswith('.')]
        return res.makeResponse(200,
                                {'buckets': buckets})
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def createUploadTicket(bucketName, objectName):
    bucketPath = "{}/{}".format(storagePath, bucketName)
    if not os.path.exists(bucketPath):
        return res.makeDoesNotExistResponse("Bucket")
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True)
        if prevent: return prevent
        db.addUploadTicketToDB(bucketName, objectName)
        return res.success
    except mysqlError as e:
        if e.errno == errorcode.ER_DUP_ENTRY: # If deleteFlag is set, user should call deleteObject again
            return res.makeAlreadyExistResponse("Object")
        return res.makeErrorResponse(400, "Error", e.msg)
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def completeUploadTicket(bucketName, objectName):
    bucketPath = "{}/{}".format(storagePath, bucketName)
    if not os.path.exists(bucketPath):
        return res.makeDoesNotExistResponse("Bucket")
    try:
        # This also checks if the object exist in DB
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsComplete=True)
        if prevent: return prevent
        objectParts = Helper.getAllObjectParts(bucketPath, objectName)
        md5Parts = db.getObjectPartsMD5(bucketName, objectName)
        partError = Verifier.checkObjectParts(bucketName, objectName, objectParts, md5Parts)
        if partError: return partError
        md5File = Helper.generateObjectMD5FromParts(md5Parts)
        fileSize = Helper.getObjectSizeFromParts(bucketName, objectParts)
        db.setObjectComplete(bucketName, objectName, md5File)
        return res.makeResponse(200,
                                {'status': "Success",
                                 'eTag': md5File,
                                 'length': fileSize,
                                 'name': objectName})
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

# TODO: Handle millions of file upload by implementing the following: https://serverfault.com/questions/95444/storing-a-million-images-in-the-filesystem
def saveObjectPartToBucket(bucketName, objectName, fileContent, part, request):
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsComplete=True)
        if prevent: return prevent
        fileName = Helper.addPartToObjectName(objectName, part)
        objectPathAbs = Helper.makeAbsPath(bucketName, fileName)
        # If there is already file, verify the file content, and return if all checksum matches
        if os.path.isfile(objectPathAbs):
            isCorrupted, fileSize, md5File = Verifier.checkFileIntegrity(objectPathAbs, request)
            if not isCorrupted:
                try: db.addObjectPartToDB(bucketName, objectName, part, md5File) # In case it failed previously
                except: pass
                return res.makeAlreadyExistResponse("Object({})".format(fileName))
        # Otherwise replace existing file with the new object
        Helper.fileSave(objectPathAbs, fileContent) # Will replace existing file if already exist
        isCorrupted, fileSize, md5File = Verifier.checkFileIntegrity(objectPathAbs, request)
        if isCorrupted: return isCorrupted
        db.addObjectPartToDB(bucketName, objectName, part, md5File) # May fail and cause other errors, should reattempt
        return res.makeResponse(200,
                                {'status': "Success",
                                 'md5': md5File,
                                 'length': fileSize,
                                 'partNumber': part})
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def saveObjectMeta(bucketName, objectName, request):
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsIncomplete=True)
        if prevent: return prevent
        db.addObjectMetaToDB(bucketName, objectName, request.args["key"], request.form["value"])
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def deleteObjectPartFromBucket(bucketName, objectName, part):
    objectPath = "{}/{}/{}".format(storagePath, bucketName, Helper.addPartToObjectName(objectName, part))
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsComplete=True)
        if prevent: return prevent
        if os.path.isfile(objectPath): os.remove(objectPath)
        db.removeObjectPartFromDB(bucketName, objectName, part)
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def deleteObjectFromBucket(bucketName, objectName):
    # If bucket is deleted, object must have been removed from both file system and DB due to deleteBucket
    bucketPath = "{}/{}".format(storagePath, bucketName)
    if not os.path.exists(bucketPath):
        return res.makeDoesNotExistResponse("Bucket")
    toDelete = Helper.getAllObjectParts(bucketPath, objectName)
    try:
        db.setObjectDeleteFlag(bucketName, objectName)
        for item in toDelete:
            os.remove("{}/{}".format(bucketPath, item))
        db.removeObjectFromDB(bucketName, objectName)
        if len(toDelete) == 0: return res.makeDoesNotExistResponse("Object")
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def deleteObjectMeta(bucketName, objectName, request):
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsIncomplete=True)
        if prevent: return prevent
        db.removeObjectMetaFromDB(bucketName, objectName, request.args["key"])
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def download(bucketName, objectName, request):
    bucketPath = "{}/{}".format(storagePath, bucketName)
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsIncomplete=True)
        if prevent: return prevent
        eTag = db.getObjectMD5(bucketName, objectName)
        #eTag guaranteed not None if completeFlag is set
        if 'If-Match' in request.headers:
            if eTag not in request.headers['If-Match']: return Response(status=416)
        if 'Range' in request.headers:
            # Only supports single threaded downloader, multi-threaded may lead to unexpected result
            # TODO: Parse range correctly, so that it will work with multi-threaded downloader
            startFrom = int(request.headers['range'].split("=")[1].split("-")[0])
        else: startFrom = 0
        toDownload = [Helper.makeAbsPath(bucketName, partName) for partName in Helper.getAllObjectParts(bucketPath, objectName)]
        totalFileSize = sum(os.path.getsize(file) for file in toDownload)
        return res.makeDownloadStreamResponse(toDownload, startFrom, objectName, totalFileSize, eTag, request)
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def getObjectMetaByKey(bucketName, objectName, key):
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsIncomplete=True)
        if prevent: return prevent
        meta = db.getObjectMetaByKey(bucketName, objectName, key)
        return res.makeResponse(200,
                                {'status': "Success",
                                 'metas': meta})
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

def getAllObjectMetas(bucketName, objectName):
    try:
        prevent = Verifier.constraintChecker(bucketName, objectName, preventIfBucketIsDeleted=True, preventIfObjectIsDeleted=True, preventIfObjectIsIncomplete=True)
        if prevent: return prevent
        metas = db.getAllObjectMetas(bucketName, objectName)
        return res.makeResponse(200,
                                {'status': "Success",
                                 'metas': metas})
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))