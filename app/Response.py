from flask import Flask, jsonify, Response
import Helper
import magic

def makeResponse(statusCode, response):
    app = Flask(__name__)
    with app.app_context():
        json = jsonify(response)
        json.status_code = statusCode
        return json

def makeForbiddenNameResponse(key):
    return makeResponse(400,
                        {'status': "Error",
                         'message': "{} name contains forbidden characters".format(key)})

def makeAlreadyExistResponse(key):
    return makeResponse(400,
                        {'status': "Error",
                         'message': '{} already exists'.format(key)})

def makeAlreadyDeletedResponse(key):
    return makeResponse(400,
                        {'status': "Error",
                         'message': '{} already deleted'.format(key)})

def makeDoesNotExistResponse(key):
    return makeResponse(400,
                        {'status': "Error",
                         'message': '{} does not exist'.format(key)})

def makeMissingRequiredResponse(key, value):
    return makeResponse(400,
                        {'status': "Error",
                         'message': "Missing required {}: {}".format(key, value)})

def makeMissingPartsResponse(parts):
    return makeResponse(400,
                        {'status': "Error",
                         'message': "Missing parts: {}".format(parts)})

def makeErrorResponse(statusCode, status, msg):
    return makeResponse(statusCode,
                        {'status': status,
                         'message': msg})

def makeMismatchedResponse(key):
    return makeResponse(400,
                        {'status': "Error",
                         'message': "{} mismatched".format(key)})

def makePartsMD5MismatchedResponse(parts):
    return makeResponse(400,
                        {'status': "Error",
                         'message': "Mismatched in md5 for following parts: {}".format(parts)})

def makeDownloadStreamResponse(toDownload, startFrom, objectName, totalFileSize, eTag, request):
    myRes = Response(Helper.fileGenerator(toDownload, startFrom), mimetype=magic.from_file(toDownload[0], mime=True))
    myRes.headers['Content-Disposition'] = 'attachment; filename={}'.format(objectName)
    myRes.headers['Accept-Ranges'] = "bytes"
    myRes.headers['Content-Length'] = totalFileSize - startFrom
    myRes.headers['ETag'] = eTag
    if 'Range' in request.headers:
        myRes.headers['Content-Range'] = "bytes {}-{}/{}".format(startFrom, totalFileSize - 1, totalFileSize)
        myRes.status = "Partial Content"
        myRes.status_code = 206
    return myRes

success = makeResponse(200,
                       {'status': "Success"})
partNumberNotANumber = makeResponse(400,
                                    {'status': "Error",
                                     'message': "Part number is not a number"})
partNumberOutOfRange = makeResponse(400,
                                    {'status': "Error",
                                     'message': "Part number out of range"})
noFileSpecified = makeResponse(400,
                               {'status': "Error",
                                'message': "No file specified"})
noPartsUploaded = makeResponse(400,
                               {'status': "Error",
                                'message': "No parts uploaded"})
missingPartsMD5 = makeResponse(400,
                               {'status': "Error",
                                'message': "md5 of some parts are missing"})
uploadAlreadyDone = makeResponse(400,
                                 {'status': "Error",
                                  'message': "Upload is already done"})
uploadNotDone = makeResponse(400,
                             {'status': "Error",
                              'message': "Upload is not done"})