-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2018 at 01:03 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `object_storage`
--
CREATE DATABASE IF NOT EXISTS `object_storage` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `object_storage`;

-- --------------------------------------------------------

--
-- Table structure for table `buckets`
--

CREATE TABLE `buckets` (
  `bucket_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `delete_flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `buckets_meta`
--

CREATE TABLE `buckets_meta` (
  `bucket_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `created` bigint(11) NOT NULL,
  `modified` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE `objects` (
  `object_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `bucket_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `md5` varchar(40) DEFAULT NULL,
  `complete_flag` tinyint(1) NOT NULL,
  `delete_flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `objects_meta`
--

CREATE TABLE `objects_meta` (
  `object_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `bucket_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `key` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `value` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `objects_parts`
--

CREATE TABLE `objects_parts` (
  `object_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `bucket_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `part_number` int(5) NOT NULL,
  `md5` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buckets`
--
ALTER TABLE `buckets`
  ADD PRIMARY KEY (`bucket_name`);

--
-- Indexes for table `buckets_meta`
--
ALTER TABLE `buckets_meta`
  ADD PRIMARY KEY (`bucket_name`);

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`object_name`,`bucket_name`),
  ADD KEY `bucket_name` (`bucket_name`);

--
-- Indexes for table `objects_meta`
--
ALTER TABLE `objects_meta`
  ADD PRIMARY KEY (`object_name`,`bucket_name`,`key`),
  ADD KEY `bucket_name` (`bucket_name`);

--
-- Indexes for table `objects_parts`
--
ALTER TABLE `objects_parts`
  ADD PRIMARY KEY (`object_name`,`bucket_name`,`part_number`),
  ADD KEY `bucket_name` (`bucket_name`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `buckets_meta`
--
ALTER TABLE `buckets_meta`
  ADD CONSTRAINT `buckets_meta_ibfk_1` FOREIGN KEY (`bucket_name`) REFERENCES `buckets` (`bucket_name`) ON DELETE CASCADE;

--
-- Constraints for table `objects`
--
ALTER TABLE `objects`
  ADD CONSTRAINT `objects_ibfk_1` FOREIGN KEY (`bucket_name`) REFERENCES `buckets` (`bucket_name`) ON DELETE CASCADE;

--
-- Constraints for table `objects_meta`
--
ALTER TABLE `objects_meta`
  ADD CONSTRAINT `objects_meta_ibfk_1` FOREIGN KEY (`object_name`) REFERENCES `objects` (`object_name`) ON DELETE CASCADE,
  ADD CONSTRAINT `objects_meta_ibfk_2` FOREIGN KEY (`bucket_name`) REFERENCES `buckets` (`bucket_name`) ON DELETE CASCADE;

--
-- Constraints for table `objects_parts`
--
ALTER TABLE `objects_parts`
  ADD CONSTRAINT `objects_parts_ibfk_1` FOREIGN KEY (`object_name`) REFERENCES `objects` (`object_name`) ON DELETE CASCADE,
  ADD CONSTRAINT `objects_parts_ibfk_2` FOREIGN KEY (`bucket_name`) REFERENCES `buckets` (`bucket_name`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
