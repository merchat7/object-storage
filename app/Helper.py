from Global import storagePath
import os
import hashlib

def makeAbsPath(bucketName, objectName):
    bucketPathAbs = os.path.dirname(os.path.realpath(__file__)) + "{}/{}/".format(storagePath[1:], bucketName)
    objectPathAbs = bucketPathAbs + objectName
    return objectPathAbs

def addPartToObjectName(objectName, part):
    # Keep original name for part 1
    if int(part) > 1: objectName = objectName + ".part{}".format(part.zfill(5))
    return objectName

def getPartNum(objectName, fileName):
    if fileName == objectName:
        return 1
    splitted = fileName.rsplit(".", 1)
    if len(splitted) == 2:
        fileName, partName = splitted
        try:
            if (fileName == objectName
                    and partName[:4] == "part"
                    and 2 <= int(partName[4:]) <= 10000):
                return int(partName[4:])
        except ValueError: pass

def getAllObjectParts(bucketPath, objectName):
    toReturn = []
    for item in os.listdir(bucketPath):
        if item == objectName:
            toReturn.append(item)
        else:
            if getPartNum(objectName, item): toReturn.append(item)
    return sorted(toReturn)

def generateObjectMD5FromParts(md5Parts):
    md5String = ""
    for md5Part in md5Parts:
        md5String += md5Part
    return hashlib.md5(md5String.encode('utf8')).hexdigest() + "-{}".format(len(md5Parts))

def getObjectSizeFromParts(bucketName, objectParts):
    totalSize = 0
    for objectPart in objectParts:
        totalSize += os.path.getsize(makeAbsPath(bucketName,objectPart))
    return totalSize

def fileGenerator(toDownload, startFrom, chunkSize=8192):
    for file in toDownload:
        fileSize = os.path.getsize(file)
        # Range="bytes:0-" and seek(0) = beginning of file
        if startFrom >= fileSize:
            startFrom -= fileSize
            continue
        with open(file, 'rb') as f:
            f.seek(startFrom)
            startFrom = 0  # Almost died because of this line
            while True:
                chunk = f.read(int(chunkSize))
                if chunk:
                    yield chunk
                else:
                    break

def fileSave(fileSavePathWithObjectName, fileContent):
    with open(fileSavePathWithObjectName, 'wb') as f:
        f.write(fileContent)
