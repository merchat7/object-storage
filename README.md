# Object Storage

Custom backend storage server similiar to Amazon S3. 

## Getting Started

### Prerequisites

- Python 3 (for packages, see requirements.txt)
- Pytest
- Locust
- Docker + Docker-Compose


### Running locally

```
# Run a local MySQL instance (e.g. XAMPP)

# Run the server
cd app
python Server.py local # local will cause server to init database schema for you
```

## Running the tests

### Correctness test
```
# Run the corectness test
cd test
pytest Test.py
```

If you have problem with importing for Test.py, try adding the following lines at the top
```
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
```

### Performance test

The performance while deployed locally in Docker for Windows is 100 request/sec and is stable with 100 users

```
# Run the performance test
cd test
locust -f Benchmark.py
```

## Deployment

```
cd app
# Clean up stopped Docker instances
docker-compose rm
# Build the Docker image
docker-compose build
# Start the Dockers instances
docker-compose up
```